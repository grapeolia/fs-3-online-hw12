package HW12;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

public class Child extends Human implements Serializable {

    private Woman mother;
    private Man father;
    public Child() {
    }

    public Child(String name, String surname, long birthDate, Sexes childSex) {
        super(name, surname, birthDate);
        setSex(childSex);
    }

    public Child(String name, String surname, long birthDate, int iq, Map<String, String> schedule, Sexes childSex) {
        super(name, surname, birthDate, iq, schedule);
        setSex(childSex);
    }

    public Child(String name, String surname, String birthDateInString, int iq, Sexes sex) {
        super(name, surname, birthDateInString, iq, sex);
    }

    public Woman getMother() {
        if(this.getFamily()!=null) {
            return this.getFamily().getMother();
        }
        return null;
    }

    public Man getFather() {
        if(this.getFamily()!=null) {
                return this.getFamily().getFather();
        }
        return null;
    }

    @Override
    public String prettyFormat(){
        String childSexPretty = "unknown";
        if(getSex() == Sexes.FEMALE) {
            childSexPretty = "girl";
        } else if (getSex() == Sexes.MALE){
            childSexPretty = "boy";
        }

        return childSexPretty+": {name='" + this.getName() + '\'' +
                ", surname='" + this.getSurname() + '\'' +
                ", DOB=" +getDOBInFormat()+
                ", iq=" + this.getIq() +
                ", schedule=" + this.getSchedule() +
                "}\n";
    }
}
