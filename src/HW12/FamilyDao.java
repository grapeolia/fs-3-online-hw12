package HW12;

import java.util.List;
import java.util.Set;

public interface FamilyDao {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int familyIndex);
    boolean deleteFamily(int familyIndex);
    void saveFamily(Family newFamily);
    List<Human> getAllChildren();
    void addChild(Family familyForChild, Child newChild);
    void deleteChild(Family familyToDeleteChild, Child child);
    void deleteAllChildrenOlderThan(int minAge);
    int count();
    Set<Pet> getPets(int familyIndex);
    void addPet(int familyIndex, Pet newPet);
}
