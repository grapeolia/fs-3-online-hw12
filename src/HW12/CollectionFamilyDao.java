package HW12;

import com.fasterxml.jackson.databind.*;
import java.io.*;
import java.nio.file.Paths;
import java.util.*;

public class CollectionFamilyDao implements FamilyDao, Serializable {
   public List<Family> families;

    public CollectionFamilyDao(List<Family> families) {
        this.families = families;
    }

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }
    @Override
    public Family getFamilyByIndex(int familyIndex) {
        if(families != null
                && familyIndex>=0
                && families.size()>familyIndex){
            return families.get(familyIndex);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int familyIndex) {
        if(families != null
                && familyIndex>=0
                && families.size()>familyIndex){
            setFamilies(new ArrayList<>(getFamilies()));
            families.remove(familyIndex);
            return true;
        }
        return false;
    }

    @Override
    public void saveFamily(Family newFamily) {
        if(families != null){
            for (Family family : families) {
                if (newFamily.equals(family)) {
                    System.out.println("Family already exist.");
                    return;
                }
            }
            } else {
            families = new ArrayList<>();
            }
        setFamilies(new ArrayList<>(getFamilies()));
        families.add(newFamily);
    }

    @Override
    public void addChild(Family familyForChild, Child newChild) {
        boolean isFamilyOnList = families != null && families.contains(familyForChild);
        if (isFamilyOnList) {
            if (familyForChild.countFamilyMembers() >= 6) {
                throw new FamilyOverflowException();
            } else {
                familyForChild.addChild(newChild);
            }
        }
    }

    @Override
    public void deleteChild(Family familyToDeleteChild, Child child) {
        familyToDeleteChild.deleteChild(child);
    }

    @Override
    public List<Human> getAllChildren() {
        List<Human> allChildren = new ArrayList<>();
        if(families != null){
            for(Family family : families){
                if(family.getChildren()!=null){
                    allChildren.addAll(family.getChildren());
                }
            }
        }
        return allChildren;
    }
    @Override
    public void deleteAllChildrenOlderThan(int minAge){
        if(getFamilies()!=null){
            getFamilies().forEach(family -> {
                if (family.getChildren()!=null){
                    family.getChildren().removeIf(child -> child.getAge()>minAge);
                }
            });
        }
    }

    @Override
    public int count() {
       return families!=null ? families.size() : 0;
    }

    @Override
    public Set<Pet> getPets(int familyIndex) {
        if(families != null && families.size()>familyIndex && families.get(familyIndex).getPets()!=null){
            return families.get(familyIndex).getPets();
        }
        return null;
    }

    @Override
    public void addPet(int familyIndex, Pet newPet) {
        if(families != null
                && families.size()>familyIndex){
            families.get(familyIndex).addPet(newPet);
        }
    }



    public void saveInJSON(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("families.json"), this.getFamilies());
            String jsonString = mapper.writeValueAsString(this.getFamilies());
            System.out.println(jsonString);
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    public void setFamiliesFromFileOrGenerate() {
        List <Family> families;
        if (!Paths.get("familiesList").toFile().exists()) {
            fillWithTestingData();
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("familiesList") ) ) {
            families = (List<Family>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        setFamilies(families);
    }

    public void loadData (){
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("familiesList"))) {
            objectOutputStream.writeObject(getFamilies());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void fillWithTestingData(){

        if (!Paths.get("familiesList").toFile().exists()) {
            Set<String> petHabits = Set.of("eat","sleep","jump");
            Pet simpleDomesticCat = new DomesticCat();
            Pet fullDog = new Dog("Tofik",2,34,petHabits);
            Map<String,String> motherSchedule = new HashMap<>() {{
                put(DayOfWeek.WEDNESDAY.name(), "fitness");
                put(DayOfWeek.FRIDAY.name(), "fitness");
            }};
            Map<String,String> fatherSchedule = new HashMap<>() {{
                put(DayOfWeek.MONDAY.name(), "football");
                put(DayOfWeek.SATURDAY.name(), "jym");
            }};
            Map<String,String> childSchedule = new HashMap<>() {{
                put(DayOfWeek.THURSDAY.name(), "English");
                put(DayOfWeek.SATURDAY.name(), "dancing");
            }};

            Man petrenkoFather = new Man("Petro", "Petrenko", -128088366000L);
            Woman petrenkoMother = new Woman("Kateryna", "Petrenko", 128088366000L,78,motherSchedule);
            Family petrenkoFamilyWithoutChildren = new Family(petrenkoMother,petrenkoFather);

            Man sydorenkoFather = new Man("Sydor", "Sydorenko", 128088366L, 100, fatherSchedule);
            Woman sydorenkoMother = new Woman("Liubov", "Sydorenko", 128088396000L,78,motherSchedule);
            Child sydorenkoBoyChild = new Child("Serhii","Sydorenko",1086310680000L,Sexes.MALE);
            List<Child> sydorenkoChildren = List.of(sydorenkoBoyChild);
            Child adoptiveChild = new Child("Maryna","Hrynko","20/03/2016",67,Sexes.FEMALE);
            Set<Pet> sydorenkoPets = Set.of(simpleDomesticCat);
            Family sydorenkoFamily = new Family(sydorenkoMother,sydorenkoFather,sydorenkoChildren,sydorenkoPets);
            sydorenkoFamily.addChild(adoptiveChild);

            Man chumakFather = new Man();
            Woman chumakMother = new Woman("Mother","Chumak",321541080000L);
            Child chumakChild2 = new Child();
            Child chumakChild3 = new Child("Bohdana", "Chumak", 1134219480000L,89,childSchedule,Sexes.FEMALE);
            Family chumakFamily = new Family();

            chumakFamily.addChild(adoptiveChild);
            chumakFamily.setMother(chumakMother);
            chumakFamily.setFather(chumakFather);
            chumakFamily.addChild(chumakChild2);
            chumakFamily.addChild(chumakChild2);
            chumakFamily.addChild(chumakChild3);

            List<Family> randomFamiliesList = List.of(chumakFamily,sydorenkoFamily,petrenkoFamilyWithoutChildren);
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("familiesList"))) {
                objectOutputStream.writeObject(randomFamiliesList);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

