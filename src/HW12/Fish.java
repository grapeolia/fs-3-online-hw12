package HW12;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;
import java.util.Set;
public final class Fish extends Pet implements Serializable {
    public Fish() {
        setSpecies(Species.FISH);
    }

    public Fish(String nickname) {
        super(nickname);
        setSpecies(Species.FISH);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.FISH);
    }
    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+getNickname()+". I miss you.");
    }

}
