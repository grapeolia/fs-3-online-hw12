package HW12;

import java.io.Serializable;
import java.util.Map;

public final class Man extends Human implements Serializable {
    public Man() {
        setSex(Sexes.MALE);
    }

    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
        setSex(Sexes.MALE);
    }

    public Man(String name, String surname, long birthDate, int iq, Map<String,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
        setSex(Sexes.MALE);
    }

    @Override
    public void greetPets() {
        super.greetPets();
        System.out.println("Woman said");
    }

    public void repairCar(){
        System.out.println("Finally! I've finished repairing my car.");
    }


}
