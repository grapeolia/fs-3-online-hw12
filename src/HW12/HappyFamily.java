package HW12;

import java.util.*;

public class HappyFamily {
    public static void main(String[] args) {
        List<Family> families = new ArrayList<>();
        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
        familyController.start();
    }
}