package HW12;

import java.io.Serializable;
import java.util.*;

public class Family implements Serializable {
    public Woman mother = new Woman();
    public Man father = new Man();
    public List<Child> children;
    public Set<Pet> pets;

    public Family() {

    }
    public Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Woman mother, Man father, List<Child> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Woman mother, Man father, List<Child> children, Set<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pets = pets;
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    public String prettyFormat(){
        StringBuilder childrenInString = new StringBuilder();
        if(getChildren()!=null && getChildren().size()>0){
            for(Child child:getChildren()){
                childrenInString.append("\t\t").append(child.prettyFormat());
            }
        } else {
            childrenInString = new StringBuilder("\t\t" + "this family doesn't have any child\n");
        }
        StringBuilder petsInString = new StringBuilder();
        if(getPets()!=null
            && getPets().size()>0) {
            for(Pet pet:getPets()){
                petsInString.append("\t\t").append(pet);
            }
        } else {
            petsInString = new StringBuilder("\t\t" + "this family doesn't have any pets");
        }
        return "family: "+"\n"+
                "\tmother: "+this.getMother().prettyFormat()+"\n"+
                "\tfather: "+this.getFather().prettyFormat()+"\n"+
                "\tchildren:\n"+children+
                "\tpets:\n"+petsInString;
    }

    public void addChild(Child newChild){
        if(getChildren() == null){
            setChildren(List.of(newChild));
        }else{
            if(!getChildren().contains(newChild)) {
                setChildren(new ArrayList<>(getChildren()));
                getChildren().add(newChild);
            }
        }
    }

    public boolean deleteChild(int childIndex){
        if (getChildren() != null
                && getChildren().size()>childIndex
                && getChildren().get(childIndex) != null) {
            setChildren(new ArrayList<>(getChildren()));
            getChildren().remove(childIndex);
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human childToDelete) {
        if (getChildren() != null
                && getChildren().size()>0
                && getChildren().contains(childToDelete)) {
            setChildren(new ArrayList<>(getChildren()));
            getChildren().remove(childToDelete);
            return true;
        }
        return false;
    }

    public void addPet(Pet newPet){
        if(pets == null) {
            pets = new HashSet<>();
        }
        pets.add(newPet);
    }

    public boolean deletePet(Pet petToDelete) {
        if (getPets() != null
                && getPets().size()>0
                && getPets().contains(petToDelete)) {
            getPets().remove(petToDelete);
            return true;
        }
        return false;
    }

    public int countFamilyMembers(){
        int countChildren = getChildren() != null
                ? getChildren().size()
                : 0;
        return 2+countChildren;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Objects.equals(children, family.children) && Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

}
