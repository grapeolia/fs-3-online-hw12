package HW12;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public abstract class Human implements Serializable {

    private Family family;
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String,String> schedule;
    private Sexes sex=Sexes.UNKNOWN;

    public Human() {

    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        setBirthDate(birthDate);
    }

    public Human(String name, String surname, long birthDate, int iq, Map<String,String> schedule) {
        this.name = name;
        this.surname = surname;
        setBirthDate(birthDate);
        setIq(iq);
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDateInString, int iq, Sexes sex) {
        this.name = name;
        this.surname = surname;
        setBirthDate(convertStringBirthDateToMilliseconds(birthDateInString));
        setIq(iq);
        this.sex = sex;
    }

    public Family getFamily(){
        return this.family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public long getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(long birthDate) {
        //-2208988740000 - Date and time (GMT): Monday, January 1, 1900 12:01:00 AM
        if(birthDate>-2208985200000L){
            this.birthDate = birthDate;
        } else {
            System.out.println("BirthDate has incorrect value. Min value is -2208985200000L (01/01/1900). Value 0 (01/01/1970) has been set by default.");
            this.birthDate=0L;
        }
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        if(iq>=0 && iq<=100){
            this.iq = iq;
        } else {
            this.iq = 0;
        }
    }


    public Map<String,String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String,String> schedule) {
        this.schedule = schedule;
    }

    public Sexes getSex() {
        return sex;
    }

    public void setSex(Sexes sex) {
        this.sex = sex;
    }

    private long convertStringBirthDateToMilliseconds(String birthDateInString){
        boolean isEnteredDateValid = checkIfDateIsValid(birthDateInString);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try{
            String birthDateInStringWithTime = birthDateInString+" 00:00:01";
            Date date = sdf.parse(birthDateInStringWithTime);
            return date.getTime();
        } catch (ParseException e) {
            System.out.println("Date must have format dd/MM/yyyy. 01/01/1970 has been set by default.");
        }
        return 0L;
    }

    private static boolean checkIfDateIsValid(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            format.parse(date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public void greetPets(){
        if (this.getFamily() != null && this.getFamily().getPets() != null) {
            String nickNames = "";
            for(Pet pet : this.getFamily().getPets()){
                if(pet.getNickname() != null && !Objects.equals(pet.getNickname(), "")) {
                    nickNames = ", " + pet.getNickname();
                }
            }
            System.out.println("Hi, " + nickNames + "!");
        } else {
            System.out.println("I don't have any pet. Or my pet doesn't have any nickname.");
        }
    }
    public void describePet(Pet myPet){
        if (this.getFamily().getPets() != null && this.getFamily().getPets().contains(myPet)) {
            for (Pet pet : this.family.getPets()) {
                if(pet == myPet){
                    String trickDescription = pet.getTrickLevel()>50
                            ? "very tricky"
                            : "barely tricky";
                    System.out.println("I've got a "+pet.getSpecies()+
                            ". It's "+pet.getAge()+
                            ", it's "+trickDescription+".");
                }
            }
        } else {
            System.out.println("I don't have this pet.");
        }
    }
    public String describeAge() {
        LocalDate humanDataOfBirthLocalDate =
                Instant.ofEpochMilli(this.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentLocalDate =
                Instant.ofEpochMilli(System.currentTimeMillis()).atZone(ZoneId.systemDefault()).toLocalDate();
        Period ageOfHuman = Period.between(humanDataOfBirthLocalDate,currentLocalDate);
        int yearsAge = ageOfHuman.getYears();
        int monthsAge = ageOfHuman.getMonths();
        int days = ageOfHuman.getDays();
        return "I'm "+yearsAge+" years, "+monthsAge+" months, "+days+" days.";
    }

    public int getAge(){
        LocalDate humanDataOfBirthLocalDate =
                Instant.ofEpochMilli(this.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentLocalDate =
                Instant.ofEpochMilli(System.currentTimeMillis()).atZone(ZoneId.systemDefault()).toLocalDate();
        Period ageOfHuman = Period.between(humanDataOfBirthLocalDate,currentLocalDate);
        return ageOfHuman.getYears();

    }

    public String getDOBInFormat(){
        Date humanDOB = new Date(this.getBirthDate());
        LocalDate humanDataOfBirthLocalDate =
                Instant.ofEpochMilli(this.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();
        int yearOfBirth = humanDataOfBirthLocalDate.getYear();
        int monthOfBirth = humanDOB.getMonth()+1;
        int dayOfBirth = humanDataOfBirthLocalDate.getDayOfMonth();
        return dayOfBirth+"/"+monthOfBirth+"/"+yearOfBirth;
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    public String prettyFormat(){
        return "{name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", DOB=" +getDOBInFormat()+
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }
}
