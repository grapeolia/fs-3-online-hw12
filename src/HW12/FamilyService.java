package HW12;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.time.ZonedDateTime;
import java.util.*;

public class FamilyService {
    private final CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }


    public List<Family> getAllFamilies (){
        return collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        if(getAllFamilies() != null){
            for (int i=0; i<getAllFamilies().size();i++) {
                int familyIndex = i+1;
                System.out.println("["+familyIndex+"]"+getAllFamilies().get(i).prettyFormat());
            }
        }
    }
    public List<Family> getFamiliesBiggerThan(int minFamilySize){
        List<Family> familiesBiggerThan = new ArrayList<>();
        if(getAllFamilies() != null){
            getAllFamilies()
                    .stream()
                    .filter(family -> family.countFamilyMembers()>minFamilySize)
                    .forEach(familiesBiggerThan::add);
        }
        return familiesBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int maxFamilySize){
        List<Family> familiesLessThan = new ArrayList<>();
        if(getAllFamilies() != null){
            getAllFamilies()
                    .stream()
                    .filter(family -> family.countFamilyMembers()<maxFamilySize)
                    .forEach(familiesLessThan::add);
        }
        return familiesLessThan;
    }

    public int countFamiliesWithMemberNumber(int familySize){
        List<Family> familiesWithExactNumbers = new ArrayList<>();
        if(getAllFamilies() != null){
            getAllFamilies()
                    .stream()
                    .filter(family -> family.countFamilyMembers() == familySize)
                    .forEach(familiesWithExactNumbers::add);
                    }
        return familiesWithExactNumbers.size();
    }

    public void createNewFamily(Woman mother, Man father){
        collectionFamilyDao.saveFamily(new Family(mother,father));
        System.out.println("The family has been created and added to families");
    }

    public void deleteFamilyByIndex(int familyIndex){
        boolean isDeleted = collectionFamilyDao.deleteFamily(familyIndex);
        if(isDeleted) {
            System.out.println("The family has been deleted");
        }else {
            System.out.println("Such family doesn't exist");
        }
    }

    public Family bornChild(Family familyForChild, String femaleName, String maleName) {
        if(familyForChild == null){
            return null;
        }
        Random rand = new Random();
        int randomValueToDefineSex = rand.nextInt(2);
        Sexes childSex = randomValueToDefineSex != 0 ? Sexes.MALE : Sexes.FEMALE;
        String childName = childSex == Sexes.MALE ? maleName : femaleName;
        long currentDate = ZonedDateTime. now(). toInstant(). toEpochMilli();
        String childSurname;
        String fathersSurname = familyForChild.getFather().getSurname();
        String mothersSurname = familyForChild.getMother().getSurname();
        childSurname = Objects.requireNonNullElseGet(fathersSurname, () -> Objects.requireNonNullElse(mothersSurname, "no surname from parent"));
        Child newChild = new Child(childName,childSurname,currentDate,childSex);
        collectionFamilyDao.addChild(familyForChild,newChild);
        return familyForChild;
    }

    public Family adoptChild(Family familyForChild, Child newChild){
        collectionFamilyDao.addChild(familyForChild,newChild);
        return familyForChild;
    }

    public int count(){
        return collectionFamilyDao.count();
    }

    public void deleteAllChildrenOlderThan(int minAge){
        collectionFamilyDao.deleteAllChildrenOlderThan(minAge);
    }

    public Family getFamilyById(int familyIndex){
        return collectionFamilyDao.getFamilyByIndex(familyIndex);
    }

    public Set<Pet> getPets(int familyIndex){
        return collectionFamilyDao.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet newPet){
        collectionFamilyDao.addPet(familyIndex,newPet);
    }


    public long convertStringBirthDateToMilliseconds(String birthDateInString){
        boolean isEnteredDateValid = checkIfDateIsValid(birthDateInString);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try{
            String birthDateInStringWithTime = birthDateInString+" 00:00:01";
            Date date = sdf.parse(birthDateInStringWithTime);
            return date.getTime();
        } catch (ParseException e) {
            //System.out.println("Date must have format dd/MM/yyyy. 01/01/1970 has been set by default.");
        }
        return 0L;
    }

    public static boolean checkIfDateIsValid(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            format.parse(date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public void loadData(){
        collectionFamilyDao.loadData();
    }

    public void saveFamiliesToJSON() {
        collectionFamilyDao.saveInJSON();
    }

    public void setRandomFamiliesFromFile(){
        collectionFamilyDao.setFamiliesFromFileOrGenerate();
    }

}
