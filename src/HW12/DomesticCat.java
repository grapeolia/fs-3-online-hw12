package HW12;

import java.io.Serializable;
import java.util.Set;

public final class DomesticCat extends Pet implements Pest, Serializable {

    public DomesticCat() {
        setSpecies(Species.DOMESTIC_CAT);
    }
    public DomesticCat(String nickname) {
        super(nickname);
        setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOMESTIC_CAT);
    }
    @Override
    public void foul() {
        System.out.println("I need to cover my tracks");
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+getNickname()+". I miss you.");
    }

}
