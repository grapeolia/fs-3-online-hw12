package HW12;

import java.io.Serializable;
import java.util.Map;

public final class Woman extends Human implements Serializable {

    public Woman() {
        setSex(Sexes.FEMALE);
    }

    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
        setSex(Sexes.FEMALE);
    }

    public Woman(String name, String surname, long birthDate, int iq, Map<String,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
        setSex(Sexes.FEMALE);
    }

    @Override
    public void greetPets() {
        super.greetPets();
        System.out.println("Woman said");
    }

    public void makeup(){
        System.out.println("I've finished make up. I'm beautiful!");
    }

}
