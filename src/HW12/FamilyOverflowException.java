package HW12;

public class FamilyOverflowException extends RuntimeException{
    public FamilyOverflowException() {
        super("The action was not processed. Family can not exceed 6 people");
    }
}
