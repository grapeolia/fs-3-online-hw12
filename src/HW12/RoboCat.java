package HW12;

import java.io.Serializable;
import java.util.Set;
public class RoboCat extends Pet implements Serializable {
    public RoboCat() {
        setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBOCAT);
    }
    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+getNickname()+". I miss you.");
    }

}
