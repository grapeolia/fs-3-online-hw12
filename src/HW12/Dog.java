package HW12;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;
import java.util.Set;


public final class Dog extends Pet implements Pest, Serializable {
    public Dog() {
        setSpecies(Species.DOG);
    }

    public Dog(String nickname) {
        super(nickname);
        setSpecies(Species.DOG);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void foul() {
        System.out.println("I need to cover my tracks");
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+getNickname()+". I miss you.");
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public String prettyFormat() {
        return super.prettyFormat();
    }




}
