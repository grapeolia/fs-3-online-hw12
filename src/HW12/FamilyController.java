package HW12;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

public class FamilyController {

    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void start() {
        System.out.println("started");
        while(true){
            System.out.println("1. Загрузить данные из файла");
            System.out.println("2. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)");
            System.out.println("3. Отобразить список семей, где количество людей больше заданного");
            System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
            System.out.println("5. Подсчитать количество семей, где количество членов равно");
            System.out.println("6. Создать новую семью");
            System.out.println("7. Удалить семью по индексу семьи в общем списке");
            System.out.println("8. Редактировать семью по индексу семьи в общем списке");
            System.out.println("9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)");
            System.out.println("10. Сохранить текущие данные в файл");
            System.out.println("11. Exit");
            Scanner scanner = new Scanner(System.in);
            if (!scanner.hasNextInt()) {
                System.out.println("Illegal input");
                continue;
            }
            int menuItem = scanner.nextInt();
            switch (menuItem) {
                case 1 -> familyService.setRandomFamiliesFromFile();
                case 2 -> familyService.displayAllFamilies();
                case 3 -> {
                    System.out.println("Please enter min family members (bigger than min will be displayed)");
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Choose another option from the menu.");
                        continue;
                    }
                    int minNumber = scanner.nextInt();
                    List<Family> familiesBiggerThan = familyService.getFamiliesBiggerThan(minNumber);
                    if (familiesBiggerThan.size() > 0) {
                        for (int i = 0; i < familiesBiggerThan.size(); i++) {
                            int familyIndex = i + 1;
                            System.out.println("[" + familyIndex + "]" + familiesBiggerThan.get(i).prettyFormat());
                        }
                    } else {
                        System.out.println("No families bigger than " + minNumber);
                    }
                }
                case 4 -> {
                    System.out.println("Please enter max family members (less than max will be displayed)");
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Choose another option from the menu.");
                        continue;
                    }
                    int maxNumber = scanner.nextInt();
                    List<Family> familiesLessThan = familyService.getFamiliesLessThan(maxNumber);
                    if (familiesLessThan.size() > 0) {
                        for (int i = 0; i < familiesLessThan.size(); i++) {
                            int familyIndex = i + 1;
                            System.out.println("[" + familyIndex + "]" + familiesLessThan.get(i).prettyFormat());
                        }
                    } else {
                        System.out.println("No families less than " + maxNumber);
                    }
                }
                case 5 -> {
                    System.out.println("Please enter exact family members (number of families will be displayed)");
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Choose another option from the menu.");
                        continue;
                    }
                    int exactNumber = scanner.nextInt();
                    int numberOfFamiliesWithExactMembers = familyService.countFamiliesWithMemberNumber(exactNumber);
                    System.out.println("There are " + numberOfFamiliesWithExactMembers + " which consists of " + exactNumber + " members.");
                }
                case 6 -> {
                    System.out.println("Please enter mother's name: ");
                    scanner.nextLine();
                    String mothersName = scanner.nextLine();
                    System.out.println("Please enter mother's surname: ");
                    String mothersSurname = scanner.nextLine();
                    System.out.println("Enter mothers DOB in format \"dd/MM/yyyy\". Please be aware, that if format is incorrect 01/01/1970 will be set by default.");
                    String mothersDOB = scanner.nextLine();
                    long mothersDOBInMS = familyService.convertStringBirthDateToMilliseconds(mothersDOB);
                    System.out.println("Please enter mothers iq: 0..100.");
                    int mothersIQ = 0;
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. IQ 0 has been set by default");
                    } else {
                        mothersIQ = scanner.nextInt();
                    }
                    System.out.println("Please enter father's name: ");
                    scanner.nextLine();
                    String fathersName = scanner.nextLine();
                    System.out.println("Please enter father's surname: ");
                    String fathersSurname = scanner.nextLine();
                    System.out.println("Enter fathers DOB in format \"dd/MM/yyyy\". Please be aware, that if format is incorrect 01/01/1970 will be set by default.");
                    String fathersDOB = scanner.nextLine();
                    long fathersDOBInMS = familyService.convertStringBirthDateToMilliseconds(fathersDOB);
                    System.out.println("Please enter fathers iq: 0..100.");
                    int fathersIQ = 0;
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. IQ 0 has been set by default");
                    } else {
                        fathersIQ = scanner.nextInt();
                    }
                    Woman mother = new Woman(mothersName, mothersSurname, mothersDOBInMS);
                    mother.setIq(mothersIQ);
                    Man father = new Man(fathersName, fathersSurname, fathersDOBInMS);
                    father.setIq(fathersIQ);
                    familyService.createNewFamily(mother, father);
                }
                case 7 -> {
                    int numberOfFamilies = familyService.count();
                    System.out.println("Please enter index of family to delete 1.." + numberOfFamilies);
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Choose another option from the menu.");
                        continue;
                    }
                    int familyIndexToDelete = scanner.nextInt()-1;
                    familyService.deleteFamilyByIndex(familyIndexToDelete);
                }
                case 8 -> {
                    int numberOfFamilies = familyService.count();
                    int familyToEdit;
                    System.out.println("1. Родить ребенка");
                    System.out.println("2. Усыновить ребенка");
                    System.out.println("3. Вернуться в главное меню");
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input");
                        continue;
                    }
                    int menuItemToEditFamily = scanner.nextInt();
                    switch (menuItemToEditFamily) {
                        case 1:
                            System.out.println("Please enter family to be edited 1.." + numberOfFamilies);
                            if (!scanner.hasNextInt()) {
                                System.out.println("Illegal input. Choose another option from the menu.");
                                continue;
                            }
                            familyToEdit = scanner.nextInt() - 1;
                            System.out.println("Please enter girls name: ");
                            scanner.nextLine();
                            String girlsName = scanner.nextLine();
                            System.out.println("Please enter boys surname: ");
                            String boysName = scanner.nextLine();
                            familyService.bornChild(familyService.getFamilyById(familyToEdit), girlsName, boysName);
                            break;
                        case 2:
                            System.out.println("Please enter family to be edited 1.." + numberOfFamilies);
                            if (!scanner.hasNextInt()) {
                                System.out.println("Illegal input. Choose another option from the menu.");
                                continue;
                            }
                            familyToEdit = scanner.nextInt() - 1;
                            System.out.println("Please enter child name: ");
                            scanner.nextLine();
                            String childName = scanner.nextLine();
                            System.out.println("Please enter child surname: ");
                            String childSurname = scanner.nextLine();
                            System.out.println("Enter child DOB in format \"dd/MM/yyyy\". Please be aware, that if format is incorrect 01/01/1970 will be set by default.");
                            String childDOB = scanner.nextLine();
                            System.out.println("Please enter mothers iq: 0..100.");
                            int childIQ = 0;
                            if (!scanner.hasNextInt()) {
                                System.out.println("Illegal input. IQ 0 has been set by default");
                            } else {
                                childIQ = scanner.nextInt();
                            }
                            Child adoptiveChild = new Child(childName, childSurname, childDOB, childIQ, Sexes.UNKNOWN);
                            familyService.adoptChild(familyService.getFamilyById(familyToEdit), adoptiveChild);
                            break;
                        case 3:
                            continue;
                        default:
                            System.out.println("Illegal input");
                    }
                }
                case 9 -> {
                    System.out.println("Please enter min age of children to be deleted (more than entered will be deleted)");
                    if (!scanner.hasNextInt()) {
                        System.out.println("Illegal input. Choose another option from the menu.");
                        continue;
                    }
                    int olderThanToBeDeleted = scanner.nextInt();
                    familyService.deleteAllChildrenOlderThan(olderThanToBeDeleted);
                }
                case 10 -> familyService.loadData();
                case 11 -> System.exit(0);
                default -> System.out.println("Illegal input");
            }

        }



        /*
        familyService.displayAllFamilies();
        familyService.deleteFamilyByIndex(3);
        familyService.deleteFamilyByIndex(2);
        familyService.displayAllFamilies();
        familyService.bornChild(familyService.getFamilyById(0),"Ania","Dima");
        Child adoptiveChild = new AdoptiveChild("Sofiia","Volia","13/10/2002", 86, Sexes.FEMALE);
        AdoptiveChild adoptiveChildWithIncorrectDobFormat = new AdoptiveChild("Martyn","Borulia","aa/03/2017", -3,Sexes.MALE);
        System.out.println(adoptiveChildWithIncorrectDobFormat);
        familyService.adoptChild(familyService.getFamilyById(0), adoptiveChild);
        System.out.println(familyService.getFamilyById(1));
        familyService.bornChild(familyService.getFamilyById(1),"Katia","Sasha");
        familyService.bornChild(familyService.getFamilyById(1),"Katia","Sasha");
        familyService.deleteAllChildrenOlderThan(10);
        familyService.displayAllFamilies();
        familyService.deleteAllChildrenOlderThan(12);
        familyService.createNewFamily(new Woman(),new Man());
        System.out.println(familyService.count());
        familyService.getFamilyById(1);
        System.out.println(familyService.getPets(1).toString());
        familyService.addPet(0,new DomesticCat());

         */
    }

}
